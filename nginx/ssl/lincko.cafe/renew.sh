#!/bin/bash
# Do not run it in dev000, must SSH as root to the host VM
certbot certonly --manual --manual-public-ip-logging-ok --preferred-challenges dns-01 --server https://acme-v02.api.letsencrypt.org/directory -d "*.brunomartin.net" -d brunomartin.net
# We use "\cp" instead of "cp" to avoid confirmation prompt
\cp /etc/letsencrypt/live/brunomartin.net/*.pem /lincko/nginx/ssl/brunomartin.net/
chown -R 11000:11000 /lincko/nginx/ssl/brunomartin.net/
docker container exec "$(docker ps -aqf 'name=lincko_nginx')" nginx -s reload
