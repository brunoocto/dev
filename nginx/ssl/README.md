## RENEW CERTIFICATION
Follow the instruction
```console
sh /lincko/nginx/ssl/brunomartin.net/renew.sh
```
Make sure that the new certificates appears in /lincko/nginx/ssl/brunomartin.net/ ,
if not, move them manually, chown 11000:11000 to the files, and reload nginx Docker.
