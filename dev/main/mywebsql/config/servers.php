<?php

/**
 * This file is a part of MyWebSQL package
 * defining more that one server here will give user the option to select a server at login time
 * Notes:
 *   Server list is used only when authentication type is LOGIN
 *
 * @file:      config/servers.php
 * @author     Samnan ur Rehman
 * @copyright  (c) 2008-2014 Samnan ur Rehman
 * @web        http://mywebsql.net
 * @license    http://mywebsql.net/license
 */

// add or remove list of servers below

// please make sure you have the proper extensions enabled in your php config
// to successfully connect to servers other than MySQL

// valid drivers types are:
// mysql4, mysql5, mysqli, sqlite, sqlite3, pgsql

// for sqlite driver:
//   'host' should be the folder name where sqlite databases are saved,
//   'user' and 'password' options should be set for additional security

// if true, a free form server name will be allowed to be entered instead of selecting
// existing one from the list
$ALLOW_CUSTOM_SERVERS = FALSE;

// if the above is true, only the following server types will be allowed
// sqlite is not recommended here, in order to avoid possible file system attacks
$ALLOW_CUSTOM_SERVER_TYPES = "mysql,sqlite3";

// A lot faster than PHP find recursive method
function find_linux($dir, $pattern)
{
  // escape any character in a string that might be used to trick
  // a shell command into executing arbitrary commands
  $dir = escapeshellcmd($dir);
  // execute "find" and return string with found files
  $files = shell_exec("find $dir -name '$pattern' -print");
  // create array from the returned string (trim will strip the last newline)
  $files = explode("\n", trim($files));
  // return array
  return $files;
}
$SERVER_LIST = array();

$sqlites = find_linux('/var/dev/' . $_SERVER['LINCKO_ENVIRONMENT'] . '/code', '*.sqlite');

foreach ($sqlites as $filename) {
  if (is_file($filename) && strpos($filename, '/node_modules/') === false && strpos($filename, '/vendor/') === false && strpos($filename, '/.Trash') === false) {
    $path_parts = pathinfo($filename);
    $basename = $path_parts['basename'];
    $key = str_replace('/var/dev/' . $_SERVER['LINCKO_ENVIRONMENT'] . '/code', '', $path_parts['dirname']);
    $SERVER_LIST[$key . '/'] = array(
      'host'     => $path_parts['dirname'],
      'driver'   => 'sqlite3',
      'user'     => 'root',
      'password' => 'Lincko360!@#'
    );
  }
}
if (count($SERVER_LIST) == 1) {
  echo array_keys($SERVER_LIST)[0];
}
