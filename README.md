####
# Modified    : May 23rd, 2021
# Created     : May 23rd, 2021
# Author      : Bruno Martin
# Email       : brunoocto@gmail.com
# Company     : Lincko
# Description : BPrepare the multi-users development environment
####

Hello,

To make this environement working, we need to install a minimum of applications.
This environemnet has been tested and running within CentOS 7 and 8, even if it should work I cannot confirm others distributions.

Install:
 - Nginx
 - Docker
 - Cerbot (if you want to use Let's encrypt)


Minimal commands (Firewall and SELINUX are disabled to simplify the installation, but of course not recommanded):
 - mkdir /serveride
 - yum install nano
 - systemctl disable firewalld
 - nano /etc/selinux/config
    SELINUX=disabled
 - reboot
 - yum install epel-release
 - yum update
 - yum install nginx
 - systemctl enable nginx
 - yum install certbot python3-certbot-nginx
 - yum install yum-utils
 - yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
 - yum install docker-ce docker-ce-cli containerd.io
 - systemctl enable docker
 - systemctl start docker
 - yum install curl
 - curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
 - chmod +x /usr/local/bin/docker-compose

Secure HTTP access for some pages/folder/files:
Generate your own username/password (here for instance: https://www.web2generators.com/apache-tools/htpasswd-generator)
And replace in this file /serveride/nginx/conf.d/.htpasswd

Add password to dev000:
 - cd /serveride/docker-compose.d/
 - mkdir .env
 - cd .env
 - nano main
    PASSWORD=MyAdminPWD
 - nano main
    bind-addr: 0.0.0.0:8080
    auth: password
    password: MyAdminPWD
    cert: "/ssl/fullchain.pem"
    cert-key: "/ssl/privkey.pem"

MyWebSQL
 - cd ./dev/main/mywebsql/
 - nano SQLITE
    root:MySQLitePWD

Start the Development Environment Manager:
 - cd /serveride
 - sh lincko

SSL
using Cerbot makes it a bit complex, I woud advise to buy a wildcard

Regards,

Bruno