####
# Modified    : November 16th, 2020
# Created     : November 16th, 2020
# Author      : Bruno Martin
# Email       : brunoocto@gmail.com
# Company     : Lincko
# Description : Basic information about the development environment
####

Hello,

You are currently working within a remote development environment.


1) Access to your IDE from anywhere via a browser (Chrome suits best), on any kind of device:
     https://{dev}-ide.brunomartin.net
   
   The IDE is Visual Studio Code v1.33.1 (Latest stable browser version), it's a Docker solution (so any other specific installation or update isn't permanent) shipped with:
     - npm 6.9.0
     - nodejs v10.16.3
     - php 7.2
     - yarn 1.19.0
     - Vue CLI 3.11.0
     - composer 1.6.3



2) Access your PHP application via this link format:
     https://{dev}-{path_to_index}.brunomartin.net
     
   {path_to_index} does define the path to your index.php file of your application, the entry point.
   Here is an example with ./demo/index.php , you can access it via this link:
     https://{dev}-demo.brunomartin.net
     
   In the case the index.php file is deeper in the folder architecture, use "-" in the subdomain, like you would use "/" as folder separation. For instance, if you want to reach ./demo/app/index.php
     https://{dev}-demo-app.brunomartin.net


3) In the case you still need direct access to the file, for file uploading, compilation on your machine, you can mount a point on your local machine.
Note that I don't really recommand to use local IDE with remote files, doing a search within a big application can take a while (minutes in some cases, it mainly depends on the network speed), browser IDE version is much more faster (like a local search). Using the same browser IDE along with common basic settings with other developers insure a better code standardization thanks to auto-formating.

   Windows: Install WinCSP (https://winscp.net/eng/download.php) then connect with the following parameters:
     - File protocol: SFTP
     - Host name: brunomartin.net
     - Port number: 2242
     - User name: {dev}
     - Password: ******
     - Advanced... > Environment > Directories > Remote directory: /code


   Linux: Install sshfs, then run:
   sshfs -p 2242 {dev}@brunomartin.net:/code /{local_path}


4) If you need a secured WebSocket port for Front Debugging (i.e. Hot module from Webpack), the port 8443 is available in your environment.


5) To limit memory usage, git feature is disabled by default in IDE configuration, but you can enable it if you need to use it.
File > Preferences > Settings > Git:Enabled


6) Please be aware that "/var/www" is a shared directory. It means that all developers and projects will see the same content. Such practice can be used to store files locally on the same machine. It should be better to work with a remote storage server instead, for instance with SFTP (https://www.php.net/manual/en/function.ssh2-sftp.php).


7) If you are in the need of upgrading Visual Studio Code, adding extensions (if the features doesn't work, or you can't find what you need), specific nginx configuration, or anything else, please send me the request: brunoocto@gmail.com


8) By default, all projects automatically added are using https for git connection, which mean you need to type your credential each time. In the case you generate a SSH key, make sure you switch to a GIT connection.
Here is an example:
 1. Generate your SSH Key, and paste the key on Gitlab website
ssh-keygen -t rsa -b 4096 -C "firstname.lastname@lincko.com"
cat ~/.ssh/id_rsa.pub
 2. Modify the origin for each project using git connection
cd /workspace/dhiraagu/octolib
git remote -v
git remote set-url origin git@gitlab.com:mygroup/myapp.git
git remote -v


9) To access databases, you have 2 different methods:
    - MySQL => https://phpmyadmin.brunomartin.net
    - SQLite => https://{dev}-sqlite.brunomartin.net


10) In case you need to open a .php which is not a index.php, a popup will be prompt to ask for credential, see your administrator to enter them.
    - https://{dev}-demo.brunomartin.net/app.php


11) You may also use software like Postman to request such PHP file, in this case, format the url like this:
    - https://{ask_for_username}:{ask_for_password}@{dev}-demo.brunomartin.net/app.php


Regards,

Bruno