"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const vscode_1 = require("vscode");
const copy_paste_1 = require("copy-paste");
function activate(context) {
    context.subscriptions.push(vscode_1.commands.registerCommand("copy-relative-path.copy", () => {
        let name;
        const { hideNotification } = vscode_1.workspace.getConfiguration("cprel");
        copy_paste_1.copy((name = vscode_1.workspace.asRelativePath(vscode_1.window.activeTextEditor.document.uri)), () => !hideNotification &&
            vscode_1.window.showInformationMessage(`Copied ${name} to clipboard! 🎉`));
    }));
}
exports.activate = activate;
function deactivate() { }
exports.deactivate = deactivate;
//# sourceMappingURL=extension.js.map