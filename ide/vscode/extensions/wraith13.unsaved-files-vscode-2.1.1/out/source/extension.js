'use strict';
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
const vscode = __importStar(require("vscode"));
const package_nls_json_1 = __importDefault(require("../package.nls.json"));
const package_nls_ja_json_1 = __importDefault(require("../package.nls.ja.json"));
const localeTableKey = JSON.parse(process.env.VSCODE_NLS_CONFIG).locale;
const localeTable = Object.assign(package_nls_json_1.default, ({
    ja: package_nls_ja_json_1.default
}[localeTableKey] || {}));
const localeString = (key) => localeTable[key] || key;
var UnsavedFiles;
(function (UnsavedFiles) {
    const applicationKey = "unsaved-files";
    let unsavedDocuments = [];
    let nextUnsavedDocument = null;
    let previousUnsavedDocument = null;
    let unsavedFilesLabel;
    let nextLabel;
    let previousLabel;
    class UnsavedFilesProvider {
        constructor() {
            this.onDidChangeTreeDataEventEmitter = new vscode.EventEmitter();
            this.onDidChangeTreeData = this.onDidChangeTreeDataEventEmitter.event;
            this.update = () => this.onDidChangeTreeDataEventEmitter.fire();
        }
        getTreeItem(element) {
            return element;
        }
        getChildren(_element) {
            //  unsavedDocuments は他の処理全般の都合でソートされており、順番がちょくちょく変わって view に表示するのに適さないので getUnsavedDocumentsSource() を直接利用する。
            const unsavedDocumentsSource = getUnsavedDocumentsSource();
            return 0 < unsavedDocumentsSource.length ?
                unsavedDocumentsSource.map(i => ({
                    label: stripDirectory(i.fileName),
                    resourceUri: i.uri,
                    description: i.isUntitled ?
                        digest(i.getText()) :
                        stripFileName(vscode.workspace.rootPath ?
                            i.fileName.replace(new RegExp("^" + vscode.workspace.rootPath.replace(/([\!\"\#\$\%\&\'\(\)\~\^\|\\\[\]\{\}\+\*\,\.\/])/g, "\\$1")), "") :
                            i.fileName)
                            .replace(/^[\/\\]*/, "")
                            .replace(/[\/\\]*$/, ""),
                    command: {
                        title: "show",
                        command: "vscode.open",
                        arguments: [i.uri]
                    },
                })) :
                [{
                        label: localeString("noUnsavedFiles.message"),
                    }];
        }
    }
    let unsavedFilesProvider = new UnsavedFilesProvider();
    const getConfiguration = (key, section = applicationKey) => {
        const configuration = vscode.workspace.getConfiguration(section);
        return key ?
            configuration[key] :
            configuration;
    };
    const getStatusBarLabel = () => getConfiguration("label", `${applicationKey}.statusBar`);
    const getStatusBarEnabled = () => getConfiguration("enabled", `${applicationKey}.statusBar`);
    const getViewOnExplorerEnabled = () => getConfiguration("enabled", `${applicationKey}.viewOnExplorer`);
    const setViewOnExplorerEnabled = (enabled) => __awaiter(this, void 0, void 0, function* () { return yield vscode.workspace.getConfiguration(`${applicationKey}.viewOnExplorer`).update("enabled", enabled, true); });
    const createStatusBarItem = (properties) => {
        const result = vscode.window.createStatusBarItem(properties.alignment);
        if (undefined !== properties.text) {
            result.text = properties.text;
        }
        if (undefined !== properties.command) {
            result.command = properties.command;
        }
        if (undefined !== properties.tooltip) {
            result.tooltip = properties.tooltip;
        }
        return result;
    };
    const showTextDocument = (textDocument) => __awaiter(this, void 0, void 0, function* () {
        return yield vscode.window.showTextDocument(textDocument, undefined);
    });
    UnsavedFiles.initialize = (context) => {
        const showCommandKey = `${applicationKey}.show`;
        const showNextCommandKey = `${applicationKey}.showNext`;
        const showPreviousCommandKey = `${applicationKey}.showPrevious`;
        context.subscriptions.push(
        //  コマンドの登録
        vscode.commands.registerCommand(showCommandKey, UnsavedFiles.show), vscode.commands.registerCommand(showNextCommandKey, UnsavedFiles.showNext), vscode.commands.registerCommand(showPreviousCommandKey, UnsavedFiles.showPrevious), vscode.commands.registerCommand(`${applicationKey}.showView`, showView), vscode.commands.registerCommand(`${applicationKey}.hideView`, hideView), 
        //  ステータスバーアイテムの登録
        unsavedFilesLabel = createStatusBarItem({
            alignment: vscode.StatusBarAlignment.Left,
            command: showCommandKey,
            tooltip: localeString("statusbar.show.tooltip")
        }), nextLabel = createStatusBarItem({
            alignment: vscode.StatusBarAlignment.Left,
            text: "$(triangle-right)",
            command: showNextCommandKey
        }), previousLabel = createStatusBarItem({
            alignment: vscode.StatusBarAlignment.Left,
            text: "$(triangle-left)",
            command: showPreviousCommandKey
        }), 
        //  TreeDataProovider の登録
        vscode.window.registerTreeDataProvider(applicationKey, unsavedFilesProvider), 
        //  イベントリスナーの登録
        vscode.window.onDidChangeActiveTextEditor(() => updateUnsavedDocumentsOrder()), vscode.workspace.onDidOpenTextDocument(() => updateUnsavedDocuments()), vscode.workspace.onDidCloseTextDocument(() => updateUnsavedDocuments()), vscode.workspace.onDidChangeTextDocument(() => updateUnsavedDocuments()), vscode.workspace.onDidSaveTextDocument(() => updateUnsavedDocuments()), vscode.workspace.onDidChangeConfiguration(() => onDidChangeConfiguration()));
        updateViewOnExplorer();
        updateUnsavedDocuments();
    };
    const getUnsavedFilesLabelText = () => [
        getConfiguration(unsavedDocuments.length <= 0 ?
            "noUnsavedFilesStatusLabel" :
            "anyUnsavedFilesStatusLabel", `${applicationKey}.statusBar`),
        getStatusBarLabel(),
        `${unsavedDocuments.length}`
    ].filter(i => 0 < i.length).join(" ");
    const getUnsavedDocumentsSource = () => vscode.workspace.textDocuments.filter(i => i.isDirty || i.isUntitled);
    const updateUnsavedDocuments = () => {
        const unsavedDocumentsSource = getUnsavedDocumentsSource();
        const oldUnsavedDocumentsFileName = unsavedDocuments
            .map(i => i.fileName);
        //  既知のドキュメントの情報を新しいオブジェクトに差し替えつつ、消えたドキュメントを間引く
        unsavedDocuments = oldUnsavedDocumentsFileName
            .map(i => unsavedDocumentsSource.find(j => j.fileName === i))
            .filter(i => undefined !== i)
            .map(i => i);
        //  既知でないドキュメントのオブジェクトを先頭に挿入
        unsavedDocuments = unsavedDocumentsSource
            .filter(i => oldUnsavedDocumentsFileName.indexOf(i.fileName) < 0)
            .concat(unsavedDocuments);
        updateUnsavedDocumentsOrder();
    };
    const updateUnsavedDocumentsOrder = () => {
        //  アクティブなドキュメントを先頭へ
        const activeTextEditor = vscode.window.activeTextEditor;
        if (activeTextEditor) {
            const activeDocument = activeTextEditor.document;
            if ((activeDocument.isDirty || activeDocument.isUntitled) &&
                unsavedDocuments[0].fileName !== activeDocument.fileName) {
                unsavedDocuments = [activeTextEditor.document]
                    .concat(unsavedDocuments.filter(i => i.fileName !== activeDocument.fileName));
            }
        }
        if (0 < unsavedDocuments.length) {
            const sortedUnsavedDocuments = unsavedDocuments
                .map(i => i) // 元の配列の順番を壊さない為に一次配列を作成する
                .sort((a, b) => a.fileName.localeCompare(b.fileName));
            const currentIndex = sortedUnsavedDocuments.map(i => i.fileName).indexOf(unsavedDocuments[0].fileName);
            nextUnsavedDocument = sortedUnsavedDocuments[(currentIndex + 1) % sortedUnsavedDocuments.length];
            previousUnsavedDocument = sortedUnsavedDocuments[(currentIndex - 1 + sortedUnsavedDocuments.length) % sortedUnsavedDocuments.length];
        }
        else {
            nextUnsavedDocument = null;
            previousUnsavedDocument = null;
        }
        UnsavedFiles.updateStatusBar();
        unsavedFilesProvider.update();
    };
    const onDidChangeConfiguration = () => {
        updateViewOnExplorer();
        UnsavedFiles.updateStatusBar();
    };
    UnsavedFiles.updateStatusBar = () => {
        if (getStatusBarEnabled()) {
            if (1 < unsavedDocuments.length && previousUnsavedDocument && nextUnsavedDocument) {
                if (undefined === previousLabel.tooltip) {
                    unsavedFilesLabel.hide();
                }
                previousLabel.tooltip = localeString("statusbar.showNext.tooltip").replace(/\{0\}/g, previousUnsavedDocument.fileName);
                previousLabel.show();
                unsavedFilesLabel.text = getUnsavedFilesLabelText();
                unsavedFilesLabel.show();
                nextLabel.tooltip = localeString("statusbar.showNext.tooltip").replace(/\{0\}/g, nextUnsavedDocument.fileName);
                nextLabel.show();
            }
            else {
                previousLabel.tooltip = undefined;
                previousLabel.hide();
                unsavedFilesLabel.text = getUnsavedFilesLabelText();
                unsavedFilesLabel.show();
                nextLabel.tooltip = undefined;
                nextLabel.hide();
            }
        }
        else {
            previousLabel.tooltip = undefined;
            previousLabel.hide();
            unsavedFilesLabel.hide();
            nextLabel.tooltip = undefined;
            nextLabel.hide();
        }
    };
    const updateViewOnExplorer = () => {
        vscode.commands.executeCommand("setContext", "showUnsavedFilesViewOnexplorer", getViewOnExplorerEnabled());
    };
    const showNoUnsavedFilesMessage = () => __awaiter(this, void 0, void 0, function* () { return yield vscode.window.showInformationMessage(localeString("noUnsavedFiles.message")); });
    const stripFileName = (path) => path.substr(0, path.length - stripDirectory(path).length);
    const stripDirectory = (path) => path.split('\\').reverse()[0].split('/').reverse()[0];
    const digest = (text) => text.replace(/\s+/g, " ").substr(0, 128);
    const showQuickPickUnsavedDocument = () => vscode.window.showQuickPick(unsavedDocuments.map(i => ({
        label: `$(primitive-dot) $(file-text) ${stripDirectory(i.fileName)}`,
        description: i.isUntitled ?
            digest(i.getText()) :
            stripFileName(i.fileName),
        detail: i.languageId,
        document: i
    })), {
        placeHolder: localeString("selectUnsavedFiles.placeHolder"),
    });
    UnsavedFiles.show = () => __awaiter(this, void 0, void 0, function* () {
        switch (unsavedDocuments.length) {
            case 0:
                yield showNoUnsavedFilesMessage();
                break;
            case 1:
                yield showTextDocument(unsavedDocuments[0]);
                break;
            default:
                const selected = yield showQuickPickUnsavedDocument();
                if (selected) {
                    yield showTextDocument(selected.document);
                }
                break;
        }
    });
    UnsavedFiles.showNext = () => __awaiter(this, void 0, void 0, function* () {
        if (nextUnsavedDocument) {
            yield showTextDocument(nextUnsavedDocument);
        }
        else {
            yield showNoUnsavedFilesMessage();
        }
    });
    UnsavedFiles.showPrevious = () => __awaiter(this, void 0, void 0, function* () {
        if (previousUnsavedDocument) {
            yield showTextDocument(previousUnsavedDocument);
        }
        else {
            yield showNoUnsavedFilesMessage();
        }
    });
    const showView = () => __awaiter(this, void 0, void 0, function* () { return yield setViewOnExplorerEnabled(true); });
    const hideView = () => __awaiter(this, void 0, void 0, function* () { return yield setViewOnExplorerEnabled(false); });
    //  dummy for test
    UnsavedFiles.roundZoom = (value) => {
        const cent = 100.0;
        return Math.round(value * cent) / cent;
    };
})(UnsavedFiles = exports.UnsavedFiles || (exports.UnsavedFiles = {}));
exports.activate = (context) => {
    UnsavedFiles.initialize(context);
};
exports.deactivate = () => {
};
//# sourceMappingURL=extension.js.map