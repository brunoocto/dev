import * as vscode from 'vscode';
export declare module UnsavedFiles {
    const initialize: (context: vscode.ExtensionContext) => void;
    const updateStatusBar: () => void;
    const show: () => Promise<void>;
    const showNext: () => Promise<void>;
    const showPrevious: () => Promise<void>;
    const roundZoom: (value: number) => number;
}
export declare const activate: (context: vscode.ExtensionContext) => void;
export declare const deactivate: () => void;
